package Test_Package_Annotation;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Request_Body;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class TestNG_Dataprovider_Sameclass {
	

	String requestBody;
	String Endpoint;
	File dir_Name;
	Response response;
	
	@DataProvider()
	public Object[][] requestBody(){
		return new Object[][]
				{
			         {"morpheus","leader"},
			         {"Vinayak","Qa"}
				};
	}
	@BeforeTest
	public void Testsetup() throws IOException {
		
		RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());
		
		System.out.println("Before Test Method Called");
		dir_Name = Utility.CreateLogDirectory("Post_Api_Log");
		Endpoint = Request_Body.Hostname() + Request_Body.Resource();
		
	}

	@Test(dataProvider = "requestBody" , description = "Data_Provider_Same_Class_Test")
	public void validator(String Req_name , String Req_job) throws IOException {
		 
		requestBody = "{\r\n"
				+ "    \"name\": \""+Req_name+"\",\r\n"
				+ "    \"job\": \""+Req_job+"\"\r\n"
				+ "}";

		response = API_Trigger.Post_trigger(Request_Body.Headername(), Request_Body.Headervalue(), requestBody,
				Endpoint);
		int statuscode = response.statusCode();
		

		String res_name = response.getBody().jsonPath().getString("name");
		System.out.println(res_name);
		String res_job = response.getBody().jsonPath().getString("job");
		System.out.println(res_job);
		String res_id = response.getBody().jsonPath().getString("id");
		System.out.println(res_id);
		String res_createdAt = response.getBody().jsonPath().getString("createdAt").substring(0, 11);
		System.out.println(res_createdAt);

		// STEP 3: PARSE THE REQUEST BODY AND SAVE IT INTO LOCAL VARIABLES
		// STEP 3.1: CREATE THE OBJECT FOR THE REQUEST BODY

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		// STEP 4: GENERATE EXPECTED DATE

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// STEP 5: USE testNG ASSERT

		AssertJUnit.assertEquals(res_name, req_name);
		AssertJUnit.assertEquals(res_job, req_job);
		AssertJUnit.assertEquals(res_createdAt, expecteddate);
		Assert.assertNotNull(res_id);
		
		Utility.evidenceFileCreator(Utility.testLogName("Post_TestCase_1"), dir_Name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
		

	}

	@AfterTest
	public void evidenceCreator() throws IOException {
		
		System.out.println("Test Execution Completed");
		
	}

}