package Common_utilities;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;  
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListenerClass implements ITestListener{
	
	ExtentSparkReporter sparkReporter;
	ExtentReports extentReport;
	ExtentTest test;
	
	public void reportconfiguration () {
		
		sparkReporter = new ExtentSparkReporter(".\\extent-report\\report.html");
		extentReport = new ExtentReports();
		
		extentReport.attachReporter(sparkReporter);
		 
		 extentReport.setSystemInfo("OS","Windows 10");
		 extentReport.setSystemInfo("user","Vinayak");
		 
		 
		 sparkReporter.config().setDocumentTitle("Extent_Report");
		 sparkReporter.config().setReportName("TestCase_ExtentReport");
		 sparkReporter.config().setTheme(Theme.DARK);
	}
	
	
	
	
	public void onStart (ITestContext result) {
		reportconfiguration();
		System.out.println("On Start Method invoked...");
	}
	
	public void onFinish (ITestContext result) {
		System.out.println("On Finished Method invoked...");
		extentReport.flush();
		
	}
	
	public void onTestFailures (ITestResult result) {
		System.out.println("Name of the Test method failed" + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.FAIL, MarkupHelper.createLabel("Name of the failed test is: "+ result.getName(), ExtentColor.RED));
	}
	
	public void onTestskipped (ITestResult result) {
		System.out.println("Name of the Test method failed" + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.SKIP, MarkupHelper.createLabel("Name of Skipped test case is: "+ result.getName(), ExtentColor.YELLOW));
		
	}
	
	public void onTestStart (ITestResult result) {
		System.out.println("Name of the Test method started" + result.getName());
	}
	
	public void onTestSuccess (ITestResult result) {
		System.out.println("Name of the Test method executed successfully" + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.PASS, MarkupHelper.createLabel("Name of the passed test case is: "+ result.getName(), ExtentColor.GREEN));
	
	}
	
	public void onTestFailedButWithSuccessPercentage(ITestResult result) {
		
	}


}
