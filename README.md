# MyRestAssured_TestNG_Framework_Project

Created the RestAssured Framework encapsulating all the below requirements:

1.To configure all the Rest API 


> Execute


> Extract response (Used RestAssured Liabrary)


> Parse the response (Used JsonPath Class of RestAssured Liabrary)


> Validate the response (Used TestNG Liabrary)


> Orchestrtion of test case using Annotation


> Generate user friendly reports using TestNG and Allure/Extent reports.

2.Framework is capable of reusability by creating common methods and centralised configuration files.
In this framework we created the Repository package which contains Environment class and Request Body class.


> Environment Class contains common data like Header name,Header Value, Hostname and Different Resource of Rest API.


> RequestBody class contains all the request bodies of different rest api methods.


3.Created the common method package for trigger and fetch response. This package contains API Trigger class and Utility Class.


> API TRIGGER- This class contains all trigger methods for All the Rest API request.


4.Utility Class- This class contains commonutility methods such as Log directory creation,Evidence file creation and Read & write data from the Excle file.


5.Created the common utilities package for generate the extent report for the framework.This package contains Extent listeners class.


6.Seperate test case class for each API methods.


7.Run the test case using TestNG annotation and TestNG runner.Below are the runner used for Testcase.

> Serial runner

> Parallel runner

> Data Driven Runner using TestNG XML.

> Extent listern runner for Extent report generation.

9. Annotations used_

> @ Test

> @ PARAMETERS

> @ BEFORE TEST

> @ AFTER TEST

> @ DATA PROVIDER

10.Generated the reports using Allure and Extent Report.

Source of API's
https://reqres.in